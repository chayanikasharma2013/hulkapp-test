<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;

class FileUploadController extends Controller
{
    public function index()
    {
        $files = File::all();

        return view('welcome', array('files'=>$files));
    }
 
    public function store(Request $request)
    {
        $file = $request->file('file');
 
        $name = $file->getClientOriginalName();
 
        $path = time().'-'.$name;
        $file->move('uploads/',$path);
 
        $save = new File;
 
        $save->name = str_replace('.pdf','',$name);
        $save->path = $path;
        $save->save();
 
    }
}
