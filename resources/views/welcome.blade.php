<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>HulkApps Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/normalize.css')}}" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="container items-top justify-center bg-gray-100 dark:bg-gray-900">
            
            <h3 class="justify-center pt-8 sm:justify-start sm:pt-0">Upload Pdf Files</h3>
            @error('file')
                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror

            <div class="dark:bg-gray-800 overflow-hidden row">
                
                <div class="col-md-4 p-6">
                    <h4 class="leading-7 float-left">Files</h4>
                    <form id="fileUploadForm" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="ml-4 leading-7 float-right upload" id="openFileInput">Upload <i class="fa fa-upload"></i></div>
                        <input type="file" accept="application/pdf" name="file" id="fileInput" style="display:none;"/>
                    </form>

                    <div class="row">
                        @if(count($files) > 0)
                            <input type="hidden" id="file" value="{{$files[0]->path}}" />
                            @foreach($files as $file)
                                <div class="col-md-12 mt-2 text-gray-600 dark:text-gray-400 bg-white files" onClick="loadContent('{{$file->path}}','{{$file->name}}')">{{$file->name}}</div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="col-md-8 p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                    <div class="flex items-center">
                        <h4 class="leading-7" id="filename">{{count($files) > 0 ? $files[0]->name : ''}}</h4>
                    </div>

                    <div class="file-container">
                        <object data="" type="application/pdf" id="filecontent" width="100%" height="100%"></object>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/common.js')}}"></script>
        @yield('javascript')
    </body>
</html>