$('#openFileInput').click(function(){ 
    $('#fileInput').trigger('click'); 
});

$("#fileInput").change(function(){
    var formData = new FormData($("#fileUploadForm")[0]);
    $.ajax({
        url: "/store",
        type: 'POST',              
        data: formData,
        processData: false,
        contentType: false,
        success: function(result)
        {
            location.reload();
        },
        error: function(data)
        {
            console.log(data);
        }
    });
});

$(document).ready(function(){
    var path = $("#file").val();
    if(path != undefined){
        loadContent(path,'');
    }
});

function loadContent(path, name){
    $("#filecontent").attr('data','');
    
    $(".files:first").addClass('active');
    
    if(name != ''){
        $(".files").each(function(){
            $(this).removeClass('active')
        })
        $("#filename").empty();
    }

    $("#filecontent").attr('data','/uploads/'+path+'#toolbar=0');

    if(name != ''){
        $("#filename").text(name);
    }
}
