## Tech Stack

Laravel 8

## Project Setup Guidelines

1. Update composer
2. Create database hulkapps
3. Rename .env.example to .env and update database details in .env file
4. Then import .sql file
5. And then run 'php artisan serve'

## About Project

To upload pdf files and display these files in html container, I make a model first for file storage and migrate. It is easy task to upload pdf files but logic is to display pdf files in html and I like this part. I didn't face any issue in this project.

## Estimated Time

Maximum 2 hours

## Pending

Nothing is pending from my side